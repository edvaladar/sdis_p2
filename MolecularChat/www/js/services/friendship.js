'use strict';

app.service('friendshipService', ['sessionService', 'ServerConfig', '$q', '$http',
        function (sessionService, ServerConfig, $q, $http) {

            this.getFriends = function (userId) {
                var deferred = $q.defer();

                $http.get(ServerConfig.baseUrl + '/friends/' + userId)
                    .then(function (result) {
                        deferred.resolve(result.data);
                    }, function (error) {
                        deferred.reject(error.data.message);
                    });

                return deferred.promise;
            };

            this.addFriend = function(mynickname, friendnickname){
                var deferred = $q.defer();

                $http.post(ServerConfig.baseUrl + '/addFriend', {
                    usernickname: mynickname,
                    usernickname2: friendnickname
                }).then(function (result) {
                    deferred.resolve(result.data);
                }, function (error) {
                    deferred.reject(error.data.message);
                });

                return deferred.promise;
            };

            this.removeFriend = function(mynickname, friendnickname){
                var deferred = $q.defer();

                $http.post(ServerConfig.baseUrl + '/removeFriend', {
                    usernickname: mynickname,
                    usernickname2: friendnickname
                }).then(function (result) {
                    deferred.resolve(result.data);
                }, function (error) {
                    deferred.reject(error.data.message);
                });

                return deferred.promise;
            };
        }]
);
