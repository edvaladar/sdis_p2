(function(angular, CryptoJS) {
    angular.module('ngCrypto', [])
        .service('crypto', function() {
            this.hash = function(data) {
                return CryptoJS.SHA3(data);
            };
        });
})(angular, CryptoJS);