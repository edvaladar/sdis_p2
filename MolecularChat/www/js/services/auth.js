'use strict';

app.service('authService', ['sessionService', 'ServerConfig', 'crypto', '$q', '$http',
        function (sessionService, ServerConfig, crypto, $q, $http) {

            /**
             * Loads the local session information about the user
             *
             * @returns {Promise} Resolves to the token data if successful, rejects otherwise
             */
            this.localLogin = function () {
                return sessionService.loadSession();
            };

            /**
             * Logs in a user in the system
             *
             * @param {String} username
             * @param {String} password
             * @returns {Promise} Resolves to the logged in user data if successful, rejects with an error otherwise
             */
            this.login = function (username, password) {
                var deferred = $q.defer();
                $http.post(ServerConfig.baseUrl + '/login', {
                    username: username,
                    password: password
                }).then(function (result) {
                    sessionService.createSession(result.data)
                        .then(function (data) {
                            deferred.resolve(data);
                        },
                        function (error) {
                            deferred.reject(error);
                        });
                }, function (error) {
                    deferred.reject(error.data.message);
                });

                return deferred.promise;
            };

            /**
             * Registers a user in the system
             *
             * @param {String} username
             * @param {String} nickname
             * @param {String} password
             * @returns {Promise} Resolves to the registered user data if successful, rejects with an error otherwise
             */
            this.register = function (username, nickname, password) {
                var deferred = $q.defer();

                $http.post(ServerConfig.baseUrl + '/register', {
                    username: username,
                    nickname: nickname,
                    password: password
                }).then(function (result) {
                    sessionService.createSession(result.data)
                    .then(function (data) {
                        deferred.resolve(data);
                    },
                    function (error) {
                        deferred.reject(error);
                    });
                }, function (error) {
                    deferred.reject(error.data.message);
                });

                return deferred.promise;
            };

            /**
             * Erases the user session information
             *
             * @returns void
             */
            this.logout = function () {
                sessionService.deleteSession();
            };
        }]
);