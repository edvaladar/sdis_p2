'use strict';

app.service('chatRoomService', ['sessionService', 'ServerConfig', '$q', '$http',
        function (sessionService, ServerConfig, $q, $http) {

            this.createChatRoom = function (chatName, desc, avatar) {
                var deferred = $q.defer();

                $http.post(ServerConfig.baseUrl + '/createChatRoom', {
                    chatName: chatName,
                    desc: desc,
                    avatar: avatar
                }).then(function (result) {
                    deferred.resolve(result.data);
                }, function (error) {
                    deferred.reject(error.data.message);
                });

                return deferred.promise;
            };

            this.addMessage = function(text, userNickname, chatName){
                var deferred = $q.defer();

                $http.post(ServerConfig.baseUrl + '/chatRoom/message', {
                    chatName: chatName,
                    userNickname: userNickname,
                    text: text
                }).then(function (result) {
                    deferred.resolve(result.data);
                }, function (error) {
                    deferred.reject(error.data.message);
                });

                return deferred.promise;
            };

            this.createModel = function(modelName, chatName, description){
                var deferred = $q.defer();

                $http.post(ServerConfig.baseUrl + '/chatRoom/createMarvinModel', {
                    chatName: chatName,
                    name: modelName,
                    description: description
                }).then(function (result) {
                    deferred.resolve(result.data);
                }, function (error) {
                    deferred.reject(error.data.message);
                });

                return deferred.promise;
            };


            this.saveModel = function(model, modelName, chatName){
                var deferred = $q.defer();

                $http.post(ServerConfig.baseUrl + '/chatRoom/updateMarvinModel', {
                    chatName: chatName,
                    modelName: modelName,
                    marvinModel: model
                }).then(function (result) {
                    deferred.resolve(result.data);
                }, function (error) {
                    deferred.reject(error.data.message);
                });

                return deferred.promise;
            };

            this.getModels = function (chatName) {
                var deferred = $q.defer();

                $http.get(ServerConfig.baseUrl + '/chatRoom/' + chatName + '/models')
                    .then(function (result) {
                        deferred.resolve(result.data);
                    }, function (error) {
                        deferred.reject(error.data.message);
                    });

                return deferred.promise;
            };

            this.getModel = function (chatName, modelName) {
                var deferred = $q.defer();

                $http.get(ServerConfig.baseUrl + '/chatRoom/' + chatName + '/model/' + modelName)
                    .then(function (result) {
                        deferred.resolve(result.data);
                    }, function (error) {
                        deferred.reject(error.data.message);
                    });

                return deferred.promise;
            };

            this.getChatRoomByName = function (chatName) {
                var deferred = $q.defer();

                $http.get(ServerConfig.baseUrl + '/chatRoom/' + chatName)
                    .then(function (result) {
                        deferred.resolve(result.data);
                    }, function (error) {
                        deferred.reject(error.data.message);
                    });

                return deferred.promise;
            };

            this.getUserChatRooms = function () {
                var deferred = $q.defer();

                $http.get(ServerConfig.baseUrl + '/users/' + sessionService.user.id + '/chatRooms').then(function (result) {
                    deferred.resolve(result.data);
                }, function (error) {
                    deferred.reject(error.data.message);
                });

                return deferred.promise;
            };

            this.addUserToChatRoom = function (chatName, username) {
                var deferred = $q.defer();

                $http.post(ServerConfig.baseUrl + '/chatRoom/addUser/',{
                    chatName: chatName,
                    username: username
                }).then(function (result) {
                    deferred.resolve(result.data);
                }, function (error) {
                    deferred.reject(error.data.message);
                });

                return deferred.promise;
            };
        }]
);
