angular.module('starter.controllers', [])

    .controller('MenuCtrl', function ($scope, sessionService, $state) {
        $scope.logout = function () {
            socket.emit('disconnect', {});
            sessionService.deleteSession();
            $state.go('login');
        }
    })
    .controller('DashCtrl', function ($scope, sessionService) {

    })

    .controller('LoginCtrl', function ($scope, authService, $state) {
        $scope.user = {};
        $scope.login = function () {
            authService.login($scope.user.username, $scope.user.password)
                .then(function () {
                    $state.go('menu.dash');
                })
                .catch(function (error) {
                    $scope.authError = error;
                });
        };

    })

    .controller('SignupCtrl', function ($scope, authService, $state) {
        $scope.user = {};
        $scope.register = function () {
            authService.register($scope.user.username, $scope.user.nickname, $scope.user.password)
                .then(function () {
                    $state.go('menu.dash');
                })
                .catch(function (error) {
                    $scope.authError = error;
                });
        };

    })

    .controller('ChatsCtrl', function ($scope, Chats, chatRoomService, $state) {
        $scope.newChat = {};
        $scope.newChat.avatar = "chatavatar1";
        /*$scope.remove = function (chat) {
         Chats.remove(chat);
         }*/

        chatRoomService.getUserChatRooms()
            .then(function (chatRooms) {
                $scope.chatRoomsAvaliable = chatRooms;
            });


        $scope.createChatRoom = function () {
            chatRoomService.createChatRoom($scope.newChat.chatName, $scope.newChat.desc, $scope.newChat.avatar)
                .then(function () {
                    chatRoomService.getUserChatRooms()
                        .then(function (chatRooms) {
                            $scope.chatRoomsAvaliable = chatRooms;
                            $scope.newChat = {};
                        });
                });
        };

    })

    .controller('MarvinDetailCtrl', function ($scope, $stateParams, chatRoomService) {
        var marvinSketcherInstance;
        MarvinJSUtil.getEditor("#sketch").then(function (sketcherInstance) {
            marvinSketcherInstance = sketcherInstance;
            marvinSketcherInstance.importStructure("mrv", $scope.model.model);//get model by name
        }, function (error) {
            alert("Loading of the sketcher failed" + error);
        });

        chatRoomService.getChatRoomByName($stateParams.chatName)
            .then(function (chatRoom) {
                $scope.chat = chatRoom;
            });

        chatRoomService.getModel($stateParams.chatName, $stateParams.modelName)
            .then(function (model) {
                $scope.model = model;
            });

        $('button.header-item').click(function () {

            if ($(this).find("span.back-text span.previous-title").text() == "Models") {
                marvinSketcherInstance.exportStructure("mrv", null).then(function (data) {
                    chatRoomService.saveModel(data, $stateParams.modelName, $scope.chat.chatName);
                });

            }
        });
    })

    .controller('ChatDetailCtrl', function ($scope, $stateParams, chatRoomService, sessionService) {

        var usersConnected = [];

        socket.emit('clientsConnected', {});

        socket.on('clientsConnected', function (data) {
            usersConnected = data;
            console.log("new user connected");
        });

        chatRoomService.getChatRoomByName($stateParams.chatName)
            .then(function (chatRoom) {
                $scope.chat = chatRoom;
                chatRoom.messages.forEach(function (message) {
                    var date = new Date(message.createdAt);

                    if ($('ul.messages li:first-child span.message_user').text() == message.User.nickname) {
                        $('ul.messages li:first-child').append('<p>' + message.text + '</p>');
                        $('ul.messages li:first-child .message_date').text(" " + date.getDate() + '/' + date.getMonth() + 1 + ' ' + date.getHours() + ':' + date.getMinutes());
                    } else {
                        var own_message = '';

                        if (sessionService.user.nickname != message.User.nickname)
                            own_message = 'other_user_message';

                        $('ul.messages').prepend('<li class="' + own_message + '">\
                                            <span class="message_user">' + message.User.nickname + '</span><span class="message_date"> ' + date.getDate() + '/' + date.getMonth() + 1 + ' ' + date.getHours() + ':' + date.getMinutes() + '</span>\
                                            <p class="content">' + message.text + '</p>\
                                         </li>');

                    }
                })

                socket.on('message', function (data) {

                    if (data.chatRoom == 'chatRoom_' + $scope.chat.chatName) {
                        var date = new Date();

                        if ($('ul.messages li:first-child span.message_user').text() == data.nickname) {
                            $('ul.messages li:first-child').append('<p>' + data.message + '</p>');
                            $('ul.messages li:first-child .message_date').text(" " + date.getDate() + '/' + date.getMonth() + 1 + ' ' + date.getHours() + ':' + date.getMinutes());
                        } else {
                            var own_message = '';

                            if (sessionService.user.nickname != data.nickname)
                                own_message = 'other_user_message';

                            $('ul.messages').prepend('<li class="' + own_message + '">\
                                            <span class="message_user">' + data.nickname + '</span><span class="message_date"> ' + date.getDate() + '/' + date.getMonth() + 1 + ' ' + date.getHours() + ':' + date.getMinutes() + '</span>\
                                            <p class="content">' + data.message + '</p>\
                                         </li>');

                        }
                    }
                });
            });
        $scope.userConnected = function (user) {
            if (usersConnected.indexOf(user.nickname) !== -1)
                return true;
            else
                return false;
        }
        $scope.addUser = function () {
            chatRoomService.addUserToChatRoom($stateParams.chatName, $('#addContributor').val())
                .then(function () {
                    chatRoomService.getChatRoomByName($stateParams.chatName)
                        .then(function (chatRoom) {
                            $scope.chat = chatRoom;
                            $('#addContributor').val("");
                        });
                });
        }

        $scope.sendMessage = function () {
            if ($('input#message').val() != '') {
                socket.emit('message', {
                    chatRoom: 'chatRoom_' + $scope.chat.chatName,
                    message: $('input#message').val()
                });

                chatRoomService.addMessage($('input#message').val(), sessionService.user.nickname, $scope.chat.chatName);

                $('input#message').val("");
            }
        }

        document.getElementById("message").addEventListener("keydown", function (e) {
            // Enter is pressed
            if (e.keyCode == 13) {
                $scope.sendMessage();
            }
        }, false);


    })

    .controller('AccountCtrl', function ($scope, friendshipService, sessionService) {
        $scope.friendsList;

        friendshipService.getFriends(sessionService.user.id)
            .then(function (friends) {
                $scope.friendsList = friends;
            });

        document.getElementById("nickname").addEventListener("keydown", function (e) {
            // Enter is pressed
            if (e.keyCode == 13) {
                $scope.addFriend();
                $('#nickname').val("");
            }
        }, false);

        $scope.removeFriend = function (nickname) {
            friendshipService.removeFriend(sessionService.user.nickname, nickname)
                .then(function () {
                    friendshipService.getFriends(sessionService.user.id)
                        .then(function (friends) {
                            $scope.friendsList = friends;
                        })

                });
        }

        $scope.addFriend = function () {
            friendshipService.addFriend(sessionService.user.nickname, $('#nickname').val())
                .then(function () {
                    friendshipService.getFriends(sessionService.user.id)
                        .then(function (friends) {
                            $scope.friendsList = friends;
                        })

                });
        }

    })

    .
    controller('ModelsCtrl', function ($scope, chatRoomService, $stateParams) {
        $scope.models;
        $scope.chatName = $stateParams.chatName;
        $scope.newModel = {};
        chatRoomService.getModels($stateParams.chatName)
            .then(function(models){
                $scope.models = models;
            })

        $scope.createModel = function () {
            chatRoomService.createModel($scope.newModel.name, $stateParams.chatName, $scope.newModel.description)
                .then(function () {
                    chatRoomService.getModels($stateParams.chatName)
                        .then(function(models){
                            $scope.models = models;
                            $scope.newModel = {};
                        })
                });
        };


    });

