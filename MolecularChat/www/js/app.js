// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'LocalForageModule', 'ngCrypto'])

    .run(function ($ionicPlatform, authService, sessionService, $location) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
            }
        });

        authService.localLogin()
        .then(function() {
            // do nothing
        })
        .catch(function() {
            $location.path('/login');
        });
    })

    .config(function ($stateProvider, $urlRouterProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider

            .state('login', {
                url: "/login",
                templateUrl: "templates/login.html",
                controller: 'LoginCtrl'
            })

            .state('signup', {
                url: "/signup",
                templateUrl: "templates/signup.html",
                controller: 'SignupCtrl'
            })


            .state('menu', {
                url: "/menu",
                abstract: true,
                templateUrl: "templates/menu.html"
            })

            // setup an abstract state for the tabs directive
            .state('tab', {
                url: "/tab",
                abstract: true,
                templateUrl: "templates/tabs.html"
            })

            // Each tab has its own nav history stack:

            .state('menu.dash', {
                url: '/dash',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/tab-dash.html',
                        controller: 'DashCtrl'
                    }
                }
            })

            .state('menu.chats', {
                url: '/chats',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/tab-chats.html',
                        controller: 'ChatsCtrl'
                    }
                }
            })
            .state('menu.models', {
                url: '/chats/:chatName/models',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/chat-models.html',
                        controller: 'ModelsCtrl'
                    }
                }
            })
            .state('menu.chat-detail', {
                url: '/chats/:chatName',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/chat-detail.html',
                        controller: 'ChatDetailCtrl'
                    }
                }
            })

            .state('menu.marvin-detail', {
                url: '/chats/:chatName/models/:modelName/marvin-editor',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/marvin-detail.html',
                        controller: 'MarvinDetailCtrl'
                    }
                }
            })

            .state('menu.account', {
                url: '/account',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/tab-account.html',
                        controller: 'AccountCtrl'
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/login');

    })
    .config(['$httpProvider', function ($httpProvider) {
        // add authentication http interceptor (adds token to the header)
        $httpProvider.interceptors.push('AuthInterceptor');
    }]);
