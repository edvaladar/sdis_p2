'use strict';

module.exports = function(server) {
    require('./auth')(server);
    require('./chatRoom')(server);
    require('./friendship')(server);

    // Serve static files
    server.route({
        method: 'GET',
        path: '/{name*}',
        handler: {
            directory: {
                path: '../MolecularChat/www'
            }
        }
    });
};