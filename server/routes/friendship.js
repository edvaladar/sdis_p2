'use strict';

var Config = require('config');
var _ = require('lodash');
var Joi = require('joi');
var Boom = require('boom');
var Moment = require('moment');
var Jwt = require('jsonwebtoken');
var Models = require('../models');

module.exports = function (server) {

    server.route({
        method: 'POST',
        path: '/api/addFriend',
        config: {
            tags: ['api'],
            auth: 'token',
            validate: {
                payload: {
                    usernickname: Joi.string().required(),
                    usernickname2: Joi.string().required()
                }
            },
            handler: function (request, reply) {
                Models.User.findOne({
                    where: {
                        nickname: request.payload.usernickname
                    }
                }).then(function (user1) {
                    if (!user1)
                        return  reply(Boom.badRequest('User1 not found'));

                    Models.User.findOne({
                        where: {
                            nickname: request.payload.usernickname2
                        }
                    }).then(function (user2) {
                        if (!user2)
                            return reply(Boom.badRequest('User2 not found'));

                        user2.addFriend(user1)
                            .then(function () {
                                user1.addFriend(user2)
                                    .then(function (data) {
                                        reply(data.dataValues);
                                    });
                            })
                    })
                })
                    .catch(function (error) {
                        console.log(error);
                        reply(Boom.badImplementation('Internal server error'));
                    });
            }
        }
    });

    server.route({
        method: 'POST',
        path: '/api/removeFriend',
        config: {
            tags: ['api'],
            auth: 'token',
            validate: {
                payload: {
                    usernickname: Joi.string().required(),
                    usernickname2: Joi.string().required()
                }
            },
            handler: function (request, reply) {
                Models.User.findOne({
                    where: {
                        nickname: request.payload.usernickname
                    }
                }).then(function (user1) {
                    if (!user1)
                        return  reply(Boom.badRequest('User1 not found'));

                    Models.User.findOne({
                        where: {
                            nickname: request.payload.usernickname2
                        }
                    }).then(function (user2) {
                        if (!user2)
                            return reply(Boom.badRequest('User2 not found'));

                        user2.removeFriend(user1)
                            .then(function () {
                                user1.removeFriend(user2)
                                    .then(function (data) {
                                        reply(data.dataValues);
                                    });
                            })
                    })
                })
                    .catch(function (error) {
                        console.log(error);
                        reply(Boom.badImplementation('Internal server error'));
                    });
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/api/friends/{id}',
        config: {
            tags: ['api'],
            auth: 'token',
            validate: {
                params: {
                    id: Joi.number().required()
                }
            },
            handler: function (request, reply) {
                Models.User.findOne({
                    where: {
                        id: request.params.id
                    }
                })
                    .then(function (user) {
                        if (!user)
                            return reply(Boom.unauthorized('No user avaliable'));

                        user.getFriends({
                            attributes: ['nickname','avatar']
                        })
                            .then(function (friends) {
                                reply(friends);
                            })
                    });

            }
        }
    });

    server.route({
        method: 'GET',
        path: '/api/users',
        config: {
            tags: ['api'],
            auth: 'token',
            validate: {
                params: {
                }
            },
            handler: function (request, reply) {
                Models.User.findAll({
                    attributes: ['nickname','avatar']
                })
                    .then(function (users) {
                        if (!users)
                            return reply(Boom.unauthorized('No user avaliable'));

                        reply(users);
                    });

            }
        }
    });

};
