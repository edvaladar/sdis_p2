'use strict';

var Config = require('config');
var _ = require('lodash');
var Joi = require('joi');
var Boom = require('boom');
var Moment = require('moment');
var Models = require('../models');

module.exports = function (server) {

    server.route({
        method: 'POST',
        path: '/api/createChatRoom',
        config: {
            tags: ['api'],
            auth: 'token',
            validate: {
                payload: {
                    chatName: Joi.string().required(),
                    desc: Joi.string().required(),
                    avatar: Joi.string()
                }
            },
            handler: function (request, reply) {
                Models.ChatRoom.findOne({
                    where: {
                        chatName: request.payload.chatName
                    }
                })
                    .then(function (chat) {
                        if (chat)
                            return reply(Boom.unauthorized('No chat room already exist!'));

                        Models.ChatRoom.create({
                            chatName: request.payload.chatName,
                            description: request.payload.desc,
                            avatar: request.payload.avatar
                        }).then(function (newChatRoom) {
                            Models.User.findOne({
                                where: {
                                    id: request.auth.credentials.id
                                }
                            }).then(function (currentUser) {
                                if (!currentUser)
                                    return reply(Boom.unauthorized('No user!'));

                                newChatRoom.addUser(currentUser);

                                reply(newChatRoom.dataValues);
                            });

                        });
                    })
                    .catch(function (error) {
                        console.log(error);
                        reply(Boom.badImplementation('Internal server error'));
                    });
            }
        }
    });

    server.route({
        method: 'POST',
        path: '/api/chatRoom/message',
        config: {
            tags: ['api'],
            auth: 'token',
            validate: {
                payload: {
                    chatName: Joi.string().required(),
                    userNickname: Joi.string().required(),
                    text: Joi.string()
                }
            },
            handler: function (request, reply) {
                Models.ChatRoom.findOne({
                    where: {
                        chatName: request.payload.chatName
                    }
                })
                    .then(function (chat) {
                        if (!chat)
                            return reply(Boom.unauthorized('Chat room doesnt exist!'));

                        Models.User.findOne({
                            where: {
                                nickname: request.payload.userNickname
                            }
                        })
                            .then(function (user) {

                                Models.Message.create({
                                    text: request.payload.text
                                }).then(function (newMessage) {
                                    user.addMessage(newMessage);
                                    chat.addMessage(newMessage);

                                    reply(newMessage.dataValues);
                                });
                            });
                    })
                    .catch(function (error) {
                        console.log(error);
                        reply(Boom.badImplementation('Internal server error'));
                    });
            }
        }
    });

    server.route({
        method: 'POST',
        path: '/api/chatRoom/createMarvinModel',
        config: {
            tags: ['api'],
            auth: 'token',
            validate: {
                payload: {
                    chatName: Joi.string().required(),
                    name: Joi.string().required(),
                    description: Joi.string().required()
                }
            },
            handler: function (request, reply) {
                Models.ChatRoom.findOne({
                        where: {
                            chatName: request.payload.chatName
                        }
                    })
                    .then(function (chat) {
                        Models.MarvinModel.create({
                            name: request.payload.name,
                            description: request.payload.description,
                            ChatRoomId: chat.id
                        })
                            .then(function(newModel){

                                if(!newModel)
                                    reply(Boom.badRequest('Cant create model'));

                                reply(newModel.dataValues);
                            })
                    })
                    .catch(function (error) {
                        console.log(error);
                        reply(Boom.badImplementation('Internal server error'));
                    });
            }
        }
    });

    server.route({
        method: 'POST',
        path: '/api/chatRoom/updateMarvinModel',
        config: {
            tags: ['api'],
            auth: 'token',
            validate: {
                payload: {
                    chatName: Joi.string().required(),
                    modelName: Joi.string().required(),
                    marvinModel: Joi.string()
                }
            },
            handler: function (request, reply) {
                Models.ChatRoom.findOne({
                        where: {
                            chatName: request.payload.chatName
                        }
                    })
                    .then(function (chat) {

                        if(!chat)
                            return reply(Boom.badRequest('Cant update a model, chat room doenst exist'));
                        Models.MarvinModel.update({
                            model:request.payload.marvinModel
                        },{
                            where:{
                                ChatRoomId: chat.id,
                                name: request.payload.modelName
                            }
                        })
                            .then(function(model){
                                reply(model.dataValues);
                            });
                    })
                    .catch(function (error) {
                        console.log(error);
                        reply(Boom.badImplementation('Internal server error'));
                    });
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/api/chatRoom/{chatName}',
        config: {
            tags: ['api'],
            auth: 'token',
            validate: {
                params: {
                    chatName: Joi.string().required()
                }
            },
            handler: function (request, reply) {
                Models.ChatRoom.findOne({
                    where: {
                        chatName: request.params.chatName
                    }
                })
                    .then(function (chat) {
                        if (!chat)
                            return reply(Boom.unauthorized('No chat room avaliable'));

                        chat.getUsers({
                            attributes: [
                                'avatar',
                                'nickname'
                            ]
                        })
                            .then(function (users) {
                                var ret = chat.dataValues;
                                ret.users = users;

                                Models.Message.findAll({
                                    attributes: [
                                        'text',
                                        'createdAt'
                                    ],
                                    where: {
                                        ChatRoomId: chat.id
                                    },
                                    order: [['createdAt', 'ASC']],
                                    include: {
                                        model: Models.User,
                                        attributes: ['nickname']
                                    }
                                }).then(function (messages) {

                                    ret.messages = messages;
                                    reply(ret);
                                });
                            });

                    })
                    .catch(function (error) {
                        console.log(error);
                        reply(Boom.badImplementation('Internal server error'));
                    });
            }
        }
    });

    server.route({
        method: 'POST',
        path: '/api/chatRoom/addUser/',
        config: {
            tags: ['api'],
            auth: 'token',
            validate: {
                payload: {
                    chatName: Joi.string().required(),
                    username: Joi.string().required()
                }
            },
            handler: function (request, reply) {
                Models.ChatRoom.findOne({
                    where: {
                        chatName: request.payload.chatName
                    }
                })
                    .then(function (chat) {
                        Models.User.findOne({
                            where: {
                                username: request.payload.username
                            }
                        })
                            .then(function (user) {
                                chat.addUser(user);
                                reply(chat.dataValues);
                            });
                    })
                    .catch(function (error) {
                        console.log(error);
                        reply(Boom.badImplementation('Internal server error'));
                    });
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/api/users/{id}/chatRooms',
        config: {
            tags: ['api'],
            auth: 'token',
            validate: {
                params: {
                    id: Joi.number().required()
                }
            },
            handler: function (request, reply) {
                Models.User.findOne({
                    where: {
                        id: request.params.id
                    }
                })
                    .then(function (user) {
                        if (!user)
                            return reply(Boom.unauthorized('No user avaliable'));

                        user.getChatRooms()
                            .then(function (chatRooms) {
                                reply(chatRooms);
                            })
                    });

            }
        }
    });

    server.route({
        method: 'GET',
        path: '/api/chatRoom/{chatName}/models',
        config: {
            tags: ['api'],
            auth: 'token',
            validate: {
                params: {
                    chatName: Joi.string().required()
                }
            },
            handler: function (request, reply) {
                Models.ChatRoom.findOne({
                    where: {
                        chatName: request.params.chatName
                    }
                })
                    .then(function (chat) {
                        if (!chat)
                            return reply(Boom.unauthorized('No chat avaliable'));

                        chat.getMarvinModels()
                            .then(function (models) {
                                reply(models);
                            })
                    });

            }
        }
    });

    server.route({
        method: 'GET',
        path: '/api/chatRoom/{chatName}/model/{modelName}',
        config: {
            tags: ['api'],
            auth: 'token',
            validate: {
                params: {
                    chatName: Joi.string().required(),
                    modelName: Joi.string().required()
                }
            },
            handler: function (request, reply) {
                Models.ChatRoom.findOne({
                    where: {
                        chatName: request.params.chatName
                    }
                })
                    .then(function (chat) {
                        if (!chat)
                            return reply(Boom.unauthorized('No chat avaliable'));

                        Models.MarvinModel.findOne({
                            where:{
                                ChatRoomId: chat.id,
                                name:request.params.modelName
                            }
                        })
                            .then(function (model) {
                                if(!model)
                                    return reply(Boom.unauthorized('No model avaliable'));

                                reply(model.dataValues);
                            })
                    });

            }
        }
    });
};