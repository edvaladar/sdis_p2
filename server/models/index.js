var Sequelize = require('sequelize');
var config    = require('config').database;  // we use node-config to handle environments

// initialize database connection
var sequelize = new Sequelize(
    config.name,
    config.username,
    config.password,
    config.options
);

// load models
var models = [
    'User',
    'ChatRoom',
    'Message',
    'MarvinModel'
];

models.forEach(function(model) {
    module.exports[model] = sequelize.import(__dirname + '/' + model);
    //module.exports[model].schema(POSTGRES_SCHEMA);
});

// describe relationships
(function(m) {
    m.User.belongsToMany(m.ChatRoom);
    m.User.belongsToMany(m.User,{as: 'friends'});
    m.ChatRoom.belongsToMany(m.User);
    m.ChatRoom.hasMany(m.Message);
    m.ChatRoom.hasMany(m.MarvinModel);
    m.User.hasMany(m.Message);
    m.Message.belongsTo(m.User);
})(module.exports);

// sync models
sequelize.sync({force:false}).then(function () {
    console.log('All models synced');
});

// export connection
module.exports.sequelize = sequelize;