module.exports = function(sequelize, DataTypes) {
    return sequelize.define('MarvinModel', {
        model: {
            type: DataTypes.TEXT
        },
        name:{
            type: DataTypes.STRING
        },
        description:{
            type:DataTypes.STRING
        }
    });
};

