module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Message', {
        text: {
            type: DataTypes.TEXT
        }
    });
};

