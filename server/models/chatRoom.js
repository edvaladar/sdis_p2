module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ChatRoom', {
        avatar: {
            type: DataTypes.STRING
        },
        chatName: {
            type: DataTypes.STRING
        },
        description: {
            type: DataTypes.STRING
        }
    });
};
