var Hapi = require('hapi');
var Joi = require('joi');

// Create a server with a host and port
var server = new Hapi.Server();
server.connection({
    port: 8000
});

var io = require('socket.io')(server.listener);

var clientsConnected = [];

io.on('connection', function (client) {

    client.on('message', function (data) {

        data.nickname = client.nickname;

        client.broadcast.emit('message', data);
        client.emit('message', data);
        console.log(data.chatRoom);
    });

    client.on('clientsConnected',function(){
        client.emit('clientsConnected',clientsConnected);
    });

    client.on('join', function (nickname) {
        client.nickname = nickname;

        if(clientsConnected.indexOf(nickname) === -1) {
            clientsConnected.push(nickname);
            client.emit('clientsConnected',clientsConnected);
        }
        console.log(clientsConnected);
    });

    client.on('disconnect', function () {

        console.log("Someone disconnected...("+client.nickname+")");

        if(clientsConnected.indexOf(client.nickname) === -1) {
            clientsConnected.slice(clientsConnected.indexOf(client.nickname),1);
        }
        console.log(clientsConnected);
    });
});

// Plugins
server.register({
    register: require('hapi-swagger'),
    options: {
        apiVersion: require('./package').version
    }
}, function (err) {
    if (err) {
        server.log(['error'], 'hapi-swagger load error: ' + err);
    } else {
        server.log(['start'], 'hapi-swagger interface loaded');
    }
});


require('./routes')(server);

// Start the server
server.start(function () {
    console.log("Server started at:", server.info.uri);
});